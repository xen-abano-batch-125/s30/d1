const express = require(`express`);
const mongoose = require(`mongoose`);
const app = express(); //creating a server
const port = 3000;

app.use(express.urlencoded({extended:true}));
app.use(express.json());

//mongoose.connect(""); way to connect MondoDB atlas connection string to our server.
//paste inside the connect(""); method the connection string copied from the mongoDB atlas db.
//remember to replace the password and the database name with actual values.
//due to updates made by MongoDB atlas developers, the default connection string is being flagged as an error, to skip that error or warning that we are going to encounter in the future, we will use the useNewParser and useUnifiedTopology objects inside our mongoose.connect.
mongoose.connect("mongodb+srv://xeniamaela:Xoden_101@cluster0.padkt.mongodb.net/b125-tasks?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true, 
		useUnifiedTopology: true
	}
).then(()=>{
	//if the mongoose succeeded on connecting, then we will print or console.log the message.
	console.log("Successfully connected to database!");
}).catch((error)=>{
	//handles error when the mongoose failed to connect on our mongodb atlas database.
	console.log(error); 
})

/*------------------------------------------------------*/

/*Schema - gives a structure of what kind of record/document we are going to contain on our database*/

//Schema() method - determines the structure of the documents to be written in the database.
//Schema acts as blueprint to our data.
//we used the Schema() constructor of the mongoose dependency to create a new Schema object for our tasks collection.
//the "new" keyword, creates a new Schema

const taskSchema = new mongoose.Schema({
	//define the fields with their corresponding data type
	//for task, it needs a field called 'name' & 'status'
	//the field name has a data type of 'String'
	//the field status has a data type of 'Boolean' with a default value of 'false'
	name: String,
	status: {
		type: Boolean,
		default: false //default value are the predefined values for a field if we dont put any value
	}
});

/*MODELS - to perform the CRUD operations for our defined collections with their corresponding schema*/
// the Task variable will contain the model for our tasks collection that shall perform the CRUD operations.
//the first parameter of the mongoose.model method indicates the collection in where to store the data. Take note: the collection name must be written in singular form and the first letter of the name must be in uppercase.
//the second parameter is used to specify the Schema of the documents that will be stored on the tasks collection.
const Task = mongoose.model('Task', taskSchema);

//MINI ACTIVITY--------------------------------------------

const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	username: String,
	password: String
});
const User = mongoose.model('User', userSchema);

/*
	Business Logic - todo list application
	-CRUD operation for our Tasks collection
*/

//insert new task
app.post("/add-task", (req,res) => {
	//call the model for our tasks collection
	//create an instance of the task model and save it to our database
	//creating a new Task with a task name 'PM Break' through the use of the Task Model
	let newTask = new Task({
		name: req.body.name
	});

	//telling our server that the newTask will now be saved as a new document to our collection on our database
	//.save() - saves a new function to our db
	//on the callback function, it will save two parameters, the error and the saved document.
	//error value shall contain the error whenever there is an error encountered while we are saving our document
	//savedTask parameter shall contain the newly saved document from the db once the saving process is successful.
	newTask.save((error,savedTask)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(`New task was saved ${savedTask}`)
		}
	});
});

//mini activity - 
app.post("/register", (req,res) => {
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		username: req.body.username,
		password: req.body.password
	});

	newUser.save((error,savedUser)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(`New user was saved. ${savedUser}`)
		}
	});
});

//retrieve tasks
app.get("/retrieve-tasks", (req,res)=> {
	//find({}) will retrieve all the documents from the tasks collection
	//the error on the callback will handle the errors encountered while retrieving the records
	//the records on the callback will handle the raw data from the database
	Task.find({}, (error, records)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(records);
		}
	});
});


//retrieve tasks that are done, means the status = true
app.get('/retrieve-tasks-done', (req, res)=>{
	//the Task Model will return all the tasks that has a status equal to true
	Task.find({ status: true }, (error, records)=> {
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	});
});

//update task operation
app.put('/complete-task/:taskId', (req,res)=>{
	//res.send({urlParams: req.params.taskId});
	//1. find the specific record
	//2. update the record

	//findByIdAndUpdate() - find record using the ID and update
	let taskId = req.params.taskId;
	//url parameters - these are the values defined on the URLs
	//to get the URL parameters - req.params.<paramsName>
	//:taskId - is a way to indicate that we are going to receive a url parameter, these are what we call "wildcard"
	Task.findByIdAndUpdate(taskId, { $set: { status: true }}, (error, updatedTask)=>{
		if(error){
			console.log(error);
		} else {
			res.send('Task completed successfully!');
		}
	});
});

//delete tasks
app.delete('/delete-task/:taskId', (req,res)=> {
	//findIdAndDelete()- finds the specific record using the ID and delete
	let taskId = req.params.taskId;
	Task.findByIdAndDelete(taskId, (error, deletedTask)=>{
		if(error){
			console.log(error);
		} else {
			res.send('Task deleted!');
		}
	});
});



//retrieve users
app.get("/retrieve-users", (req,res)=> {
	User.find({}, (error, userList)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(userList);
		}
	});
});

//delete tasks
app.delete('/delete-user/:userId', (req,res)=> {

	let userId = req.params.userId;
	User.findByIdAndDelete(userId, (error, deletedUser)=>{
		if(error){
			console.log(error);
		} else {
			res.send('User deleted!');
		}
	});
});

//update task operation
app.put('/update-username/:userId', (req,res)=>{
	res.send({urlParams: req.params.userId});
	
	let userId = req.params.userId;

	User.findOneAndUpdate(userId, { $set: { username: "Apple" }}, (error, updatedUsername)=>{
		if(error){
			console.log(error);
		} else {
			res.send('Username updated successfully!');
		}
	});
});

app.listen(port, ()=> console.log(`Server is running at ${port}`));